#!/usr/bin/env bash

#checkout
echo $(pwd)
cd /home/ubuntu/circleci-dep
echo $(pwd)
#install the requirements
source venv/bin/activate
pip install -r requirements/dev.txt
#do migration
python manage.py db upgrade
python manage.py db migrate

#run server
python manage.py runserver